package checktwo;

import driver.TestRunnerTestEngineer;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.TestPage;

/**
 * The type Check two button.
 */
public class CheckTwoButton extends TestRunnerTestEngineer {

    @Test(description = "Select dropdown list and click check2 button")
    public void selectAndClickCheck2() {
        TestPage testPage = new TestPage(driver);

        testPage.openPage("testing.html");
        Assert.assertEquals("The Test Room | Test Page", testPage.returnTitle());
        testPage.selectDay("Today");
        testPage.selectButton("Check 2");
        Assert.assertEquals("CONGRATULATIONS, AVAILABLE",testPage.returnMessage());
    }
}
