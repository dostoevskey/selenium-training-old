package utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URL;
import java.util.Properties;

/**
 * The type Properties Reader
 */
public class PropertiesReader {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(PropertiesReader.class);

    private static final String PROP_FILE = "testdata.properties";

    private static final Properties PROPERTIES;

    public PropertiesReader() {

    }

    static {
        PROPERTIES = new Properties();
        URL props = ClassLoader.getSystemResource(PROP_FILE);

        try {
            PROPERTIES.load(props.openStream());
        } catch (IOException ex) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(ex.getClass().getName() + "Properties method");
            }
        }
    }

    public static String getProperty(final String name) {

        return PROPERTIES.getProperty(name);
    }

    public static String loadProperty(final String name) {

        return getProperty(name);
    }
}
