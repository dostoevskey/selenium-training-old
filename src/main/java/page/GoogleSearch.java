package page;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * The type Google search.
 */
public class GoogleSearch extends PageObject {

   private WebDriverWait webDriverWait = new WebDriverWait(driver, 5);

    @FindBy(name = "q")
    @CacheLookup
    private WebElement searchFieldLocator;

    @FindBy(name = "btnK")
    @CacheLookup
    private WebElement googleSearchButton;

    @FindBy(css = "#rso .g")
    @CacheLookup
    private List<WebElement> linkResultsOutput;

    /**
     * Instantiates a new Google search.
     *
     * @param driver the driver
     */
    public GoogleSearch(final WebDriver driver) {
        super(driver);
    }

    /**
     * Return list of output int.
     *
     * @return the int
     */
    public int returnListOfOutput() {
        return linkResultsOutput.size();
    }

    /**
     * Search by word.
     *
     * @param typedName the typed name
     */
    public void searchByWord(final String typedName) {
        waitUntilWebelement(searchFieldLocator);
        searchFieldLocator.sendKeys(typedName);
        waitUntilWebelement(googleSearchButton);
        googleSearchButton.click();
    }

    private void waitUntilWebelement(final WebElement elementLocator) {
       webDriverWait.until(ExpectedConditions.visibilityOf(elementLocator));
    }

    /**
     * Wait until all page elements are loaded.
     */
    public void waitUntilAllPageElementsAreLoaded() {
        ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
    }
}