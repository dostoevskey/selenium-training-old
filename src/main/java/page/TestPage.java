package page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * The type Test page.
 */
public class TestPage extends PageObject {

    @FindBy(css = "#start_select")
    @CacheLookup
    private WebElement dropDown;

    @FindBy(css = "select option[value='today']")
    @CacheLookup
    private WebElement selectYesterday;

    @FindBy(css = "select option[value='weekly']")
    @CacheLookup
    private WebElement selectToday;

    @FindBy(css = "select option[value='month']")
    @CacheLookup
    private WebElement selectTomorrow;

    /**
     * The Button check 1.
     */
    @FindBy(css = "#check_first_btn")
    @CacheLookup
    public WebElement buttonCheck1;

    @FindBy(css = "#check_second_btn")
    @CacheLookup
    private WebElement buttonCheck2;

    /**
     * Instantiates a new Test with us.
     *
     * @param driver the driver
     */
    public TestPage(final WebDriver driver) {
        super(driver);
    }

    private WebDriverWait webDriverWait = new WebDriverWait(driver, 5);

    /**
     * Select day.
     *
     * @param day the day
     */
    public void selectDay(String day) {
        dropDown.click();
        if (day == "Yesterday") {
            selectYesterday.click();
        } else if (day == "Today") {
            selectToday.click();
        } else if (day == "Tomorrow") {
            selectTomorrow.click();
        } else {
            System.out.println("Please pick up day correctly");
        }
    }

    /**
     * Select button.
     *
     * @param button the button
     */
    public void selectButton(String button) {
        if (button == "Check 1") {
            buttonCheck1.click();
            waitUntilWebElementIsVisible(resultMessage);
        } else if (button == "Check 2") {
            buttonCheck2.click();
            waitUntilWebElementIsVisible(resultMessage);
        } else {
            System.out.println("Please select correct button");
        }
    }

    private void waitUntilWebElementIsVisible(final WebElement WebElementId) {
        webDriverWait.until(ExpectedConditions.visibilityOf(WebElementId));
    }
}
